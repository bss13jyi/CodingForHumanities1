## Introduction

Hey everyone! My name is Donatus and I am a graduate student in German Literature and Philosophy. I became interested in Digital Humanities via my research on contemporary writing as well as its predecessors.

### Expectations

What I expect from this course is to get in touch with some of the most important digital tools to work with text. On the one hand, I would like to be able to work with them myself primarily for matters of research. On the other hand, I would like to be able to understand the tools used by contemporary writers for their projects.

Exemplary projects:

* [Gregor Weichbrodt / Vicki Bennett – The Fundamental Questions (2014)] (http://0x0a.li/en/text/the-fundamental-questions/)

* [Michael Mandiberg – Print Wikipedia (2015)] (http://printwikipedia.com/#/about)

* [Gregor Weichbrodt – Dictionary of non-notable Artists (2016)] (http://0x0a.li/en/text/dictionary-of-non-notable-artists/)